h1. Object#probably

h2. What

@Object#probably@ lets us write:

<pre syntax="ruby">
@street_name = Location.probably.find(:first, ...elided... ).street_name.or("Anonymous")
or
@street_name = Probably.find(Location, first: ...elided...).street_name.or("Anonymous")
h2. Contact

Comments are welcome. Send an email to "Yi Wei":mailto:yiwei.in.cyber@gmail.com.
