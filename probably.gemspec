Gem::Specification.new do |s|
  s.name = "probably"
  s.version = "0.1.0"
  s.summary = "Complex navigation in most natural way"
  s.email = "yiwei.in.cyber@gmail.com"
  s.homepage = "http://github.com/zeninpalm/probably/"
  s.description = ""
  s.authors = ["Yi Wei"]
  s.files = ["History.txt",
             "License.txt",
             "README.textile",
             "lib/probably.rb",
             "lib/probably/version.rb"]
  s.test_files = [
      "spec/probably_spec.rb",
      "spec/spec_helper.rb"]
  s.rdoc_options = ["--main", "README.textile"]
  s.extra_rdoc_files = ["History.txt", "README.textile"]
end
